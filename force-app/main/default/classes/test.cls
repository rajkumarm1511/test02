/**************************************************************************************************
Name        :  ftr_VologyOpptyPresalesEmailService
Developer   :  RajuM
Description :  This Service is a Future callout to get send the email notification to Vology on SE Assinged on oppty
****************************************************************************************************/

global with sharing class ftr_VologyOpptyPresalesEmailService {       
    
    
    public static string JSONRequestMethod (List<Opportunity> lstOppRec){
        
        // prepare the request body            
        // get the EmailTo value from Custom Metadatatype is VologyEmailTo and convert to a List        
        String emailTo;
        String emailCc;
        String emailBcc;
        //String[] emailToIds,emailCcIds,emailBccIds; // 3-seperate lists 
        List<String> emailToIds = new List<String>();
        List<String> emailCcIds = new List<String>();
        String stringJson;
        String SEemailId;
        
        if(string.isNotBlank(lstOppRec[0].SEER_Assigned_To__r.email)){
        	SEemailId= lstOppRec[0].SEER_Assigned_To__r.email;
          }else{
            SEemailId ='';
          }
        
        VologyEmailTo__mdt lstEmailTo =[Select DeveloperName,EmailTo__c,EmailCc__c,EmailBcc__c from VologyEmailTo__mdt where DeveloperName = 'PresalesEmailTo' LIMIT 1];
        
        if(string.isNotBlank(lstEmailTo.EmailTo__c)){
            emailTo = lstEmailTo.EmailTo__c;
            emailToIds = emailTo.split(',');       
        }         
        System.debug('emailTo========='+emailTo);
        System.debug('emailToIds=========='+emailToIds);
        
        if(string.isNotBlank(lstEmailTo.EmailCc__c)){
        	emailCc = lstEmailTo.EmailCc__c;
        	emailCcIds = emailCc.split(',');            
        }
        
        if(string.isNotBlank(SEemailId)){
        	emailCcIds.add(SEemailId);
         }
        
        system.debug('emailCcIds========='+emailCcIds);
        /*
        if(string.isNotBlank(lstEmailTo.EmailBcc__c)){
        emailBcc = lstEmailTo.EmailBcc__c;
        emailBccIds = emailBcc.split(',');       
        } */          
        
        //try{            
                
                JSONGenerator jsonGen = JSON.createGenerator(true);
                jsonGen.writeStartObject();
                
                if(!emailToIds.isEmpty() && emailToIds != null ){
                    jsonGen.writeObjectField('emailTo', emailToIds);  
                }else{
                    jsonGen.writeObjectField('emailTo', '');  
                }
                //jsonGen.writeObjectField('emailCc','');     
                
        		if(!emailCcIds.isEmpty() && emailCcIds != null ){
                    jsonGen.writeObjectField('emailCc', emailCcIds);  
                }else{
                    jsonGen.writeObjectField('emailCc',''); 
                }       
        
        
        		jsonGen.writeObjectField('emailBcc','');
            
                if(string.isNotBlank(lstOppRec[0].SEER_Assigned_To__r.Id) ){            
                    jsonGen.writeStringField('seUserId', lstOppRec[0].SEER_Assigned_To__r.Id);
                }else{
                    jsonGen.writeStringField('seUserId', '');
                }
            
                if(string.isNotBlank(lstOppRec[0].SEER_Assigned_To__r.email)){
                    jsonGen.writeStringField('seEmailId', lstOppRec[0].SEER_Assigned_To__r.email);
                }else{
                    jsonGen.writeStringField('seEmailId', '');
                }
            
                if(string.isNotBlank(lstOppRec[0].id))
                {   
                    jsonGen.writeStringField('opportunityId', lstOppRec[0].id);
                }else{
                    jsonGen.writeStringField('opportunityId', '');
                }
            
                if(string.isNotBlank(lstOppRec[0].Name)){
                    jsonGen.writeStringField('opportunityName', lstOppRec[0].Name);
                }else{
                    jsonGen.writeStringField('opportunityName', '');
                }
            
                //if(string.isNotBlank(lstOppRec[0].Account.Name)){
                //    jsonGen.writeStringField('customerFirstName', lstOppRec[0].Account.Name);
                //}else{
                jsonGen.writeStringField('customerFirstName', '');
                //} 
            
				if(string.isNotBlank(lstOppRec[0].Account.Name)){
                    jsonGen.writeStringField('customerLastName', lstOppRec[0].Account.Name);
                }else{
                    jsonGen.writeStringField('customerLastName', '');
                }
            	jsonGen.writeFieldName('customerAddress');            
                jsonGen.writeStartObject(); 
            
                if(string.isNotBlank(lstOppRec[0].Account.ShippingStreet)){
                    jsonGen.writeStringField('addressLine1', lstOppRec[0].Account.ShippingStreet);
                }else{
                    jsonGen.writeStringField('addressLine1', '');
                }  
             	jsonGen.writeStringField('addressLine2', '');
            
            	if(string.isNotBlank(lstOppRec[0].Account.ShippingCity)){
                    jsonGen.writeStringField('city', lstOppRec[0].Account.ShippingCity);
                }else{
                    jsonGen.writeStringField('city', '');
                }  
            	
            	if(string.isNotBlank(lstOppRec[0].Account.ShippingCity)){
                    jsonGen.writeStringField('city', lstOppRec[0].Account.ShippingCity);
                }else{
                    jsonGen.writeStringField('city', '');
                } 
				
            	//jsonGen.writeStringField('state', 'TX'); //lstOppty[0].Account.ShippingState
        		if(string.isNotBlank(lstOppRec[0].Account.ShippingStateCode)){
                    jsonGen.writeStringField('state', lstOppRec[0].Account.ShippingStateCode);
                }else{
                    jsonGen.writeStringField('state', '');
                } 
                	
            	if(string.isNotBlank(lstOppRec[0].Account.ShippingPostalCode)){
                    jsonGen.writeStringField('zipCode', lstOppRec[0].Account.ShippingPostalCode);
                }else{
                    jsonGen.writeStringField('zipCode', '');
                }
            
                jsonGen.writeEndObject();
            
            	if(string.isNotBlank(lstOppRec[0].Description__c)){
                    jsonGen.writeStringField('customerNeedDescription', lstOppRec[0].Description__c);
                }else{
                    jsonGen.writeStringField('customerNeedDescription', '');
                }  
            
            	if(string.isNotBlank(lstOppRec[0].Lead_Product_List__c)){
                    jsonGen.writeStringField('leadProductList', lstOppRec[0].Lead_Product_List__c);
                }else{
                    jsonGen.writeStringField('leadProductList', '');
                }          
                
                jsonGen.writeEndObject();
                
                stringJson = jsonGen.getAsString(); 
           		System.debug('stringJson========'+stringJson);
            	return stringJson; 
            	
        //}
       //     Catch(Exception ex){
                
       //         ftr_ErrorLogHandler.insertErrorLogs('ftr_VologyOpptyPresalesEmailService','sendVologyOpptyPresalesEmail', ex);
        //        return null;
        //    }
        }
    
}